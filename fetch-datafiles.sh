#!/bin/bash

# API Documentation here: https://voxxedlu2024.cfp.dev/swagger-ui/index.html

set -e -x

mkdir -p avatars

curl -sk https://voxxedlu2024.cfp.dev/api/public/rooms > rooms.json
curl -sk https://voxxedlu2024.cfp.dev/api/public/schedules/thursday > slots-day1.json
curl -sk https://voxxedlu2024.cfp.dev/api/public/schedules/friday  > slots-day2.json

curl -sk 'https://voxxedlu2024.cfp.dev/api/public/speakers?page=0&size=1000' > speakers.json

for item in $(cat speakers.json | sed -e 's/[{,]/\n/g' | grep -e '"imageUrl"' -e '"id"' | sed -e 's/.*":"*//' -e 's/"*$//g')
do
  case $item in
  http*)
    curl -Lsk $item > avatars/${SPEAKER_UUID};;
  *)
    SPEAKER_UUID=$item;;
  esac
done

echo "Fetching completed"