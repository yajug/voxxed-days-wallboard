FROM node:22-bookworm AS builder

RUN mkdir /runtime
COPY wallboard-app /runtime/
WORKDIR /runtime

RUN rm -rf public/avatars/ src/data/
COPY fetch-datafiles.sh /usr/local/bin/
RUN cd src && mkdir -p data && cd data && fetch-datafiles.sh && mv avatars ../../public/

RUN rm -rf ./node_modules/
RUN npm install
RUN npm run build


FROM httpd:2.4-alpine

COPY --from=builder /runtime/dist /usr/local/apache2/htdocs/
COPY httpd.conf /usr/local/apache2/conf/httpd.conf

EXPOSE 80
