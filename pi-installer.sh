#!/bin/bash

# Download the Docker installation script
curl -fsSL https://get.docker.com -o get-docker.sh

# Execute the Docker installation script
sudo sh get-docker.sh

# Add the current user to the docker group
sudo usermod -aG docker $USER

# Clone the voxxed-days-wallboard repository
git clone https://gitlab.com/yajug/voxxed-days-wallboard/

# Navigate to the cloned repository
cd voxxed-days-wallboard/

# Start the Docker services defined in the docker-compose.yml file
sudo docker compose -f docker-compose.yml up -d

# Completion message
echo "Installation completed and services are started."