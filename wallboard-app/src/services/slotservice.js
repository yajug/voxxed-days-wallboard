import slotsDay1 from '@/data/slots-day1.json'
import slotsDay2 from '@/data/slots-day2.json'

import {timeAsString, weekDayName} from "@/utils/dateformats.js";

const slotService = Object.create({
    _allSlots: undefined,

    getSlot(id) {
        const allSlots = this.getSlots();

        for (let i in allSlots) {
            let slot = allSlots[i];
            if (slot.id === id) {
                return slot;
            }
        }

        return null;
    },

    getSlots() {
        if (!this._allSlots) {
            this._allSlots = slotsDay1.concat(slotsDay2);

            for (let i in this._allSlots) {
                let slot = this._allSlots[i];

                slot.day = weekDayName(new Date(slot.fromDate));
                slot.fromTime = timeAsString(new Date(slot.fromDate));
                slot.toTime = timeAsString(new Date(slot.toDate));
            }
        }


        return this._allSlots;
    },

    getCurrentSlot(roomId, dayname, time) {
        let result = null;
        const allSlots = this.getSlots();

        for (let i in allSlots) {
            let slot = allSlots[i];

            if (slot.room.id === Number(roomId)
                && slot.day === dayname
                && slot.fromTime <= time && slot.toTime > time) {
                result = slot;
                break;
            }
        }

        console.log('Current slot for room', roomId, dayname, time, 'is', result);

        return result;
    },

    getFollowingSlot(roomId, dayname, time) {
        let result = null;
        const allSlots = this.getSlots();

        for (let i in allSlots) {
            let slot = allSlots[i];

            if (slot.room.id === Number(roomId)
                && slot.day === dayname
                && slot.fromTime > time
                && slot.proposal
                && (result == null || slot.fromTime < result.fromTime)) {
                result = slot;
            }
        }

        return result;
    },

    getFollowingSlots(dayname, time, limit) {
        let result = [];
        const allSlots = this.getSlots();

        for (let i in allSlots) {
            let slot = allSlots[i];

            if (slot.day === dayname
                && slot.fromTime > time
                && slot.proposal) {
                result.push(slot);
                if (result.length === limit) {
                    break;
                }
            }
        }

        return result;
    },

    getCurrentSlots(dayname, time) {
        let result = [];
        const allSlots = this.getSlots();

        for (let i in allSlots) {
            let slot = allSlots[i];

            if (slot.day === dayname
                && slot.fromTime <= time
                && slot.toTime > time
                && slot.proposal) {
                result.push(slot);
            }
        }

        return result;
    },

    getTalk(talkId) {
        const allSlots = this.getSlots();
        let result = null;

        for (let i in allSlots) {
            let slot = allSlots[i];

            if (slot.proposal && slot.proposal.id === talkId) {
                result = slot.proposal;
                break;
            }
        }

        return result;
    }
});

export default slotService

