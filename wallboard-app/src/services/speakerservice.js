import speakers from '@/data/speakers.json'

const speakerService = Object.create({
    getSpeakers() {
        return speakers;
    },

    getSpeaker(speakerId) {
        let speakers = this.getSpeakers();

        let result = null;

        for (let i in speakers) {
            let speaker = speakers[i];
            // noinspection EqualityComparisonWithCoercionJS
            if (speaker.id == speakerId) {
                result = speaker;
                break;
            }
        }

        console.log('Speaker', speakerId, 'is', result);

        return result;
    }
});

export default speakerService
