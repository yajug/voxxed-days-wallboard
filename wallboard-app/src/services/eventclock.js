import {reactive} from 'vue'

import {timeAsString, weekDayName} from "@/utils/dateformats.js";

// noinspection PointlessArithmeticExpressionJS
const eventClock = Object.create({

    getCurrentDate() {
        return new Date(Date.now());
    },

    getTimeAsString() {
        const now = this.getCurrentDate();
        return timeAsString(now);
    },

    getWeekDayName() {
        const d = this.getCurrentDate();
        return weekDayName(d);
    },

    currentTime: reactive({hhmm: 'xx:xx', dayname: ''}),

    init() {
        setTimeout(() => {
            this.tick_wrapper();
        }, 700);
    },

    tick_wrapper() {
        this.tick();
        this.init();
    },

    setDebugTime(t) {
        this.debugTime = t;
        this.tick();
    },

    setDebugDayOfWeek(dow) {
        this.debugDay = dow;
        this.tick();
    },

    tick() {
        if (this.debugTime) {
            this.currentTime.hhmm = this.debugTime;
        } else {
            this.currentTime.hhmm = this.getTimeAsString();
        }

        if (this.debugDay) {
            this.currentTime.dayname = this.debugDay;
        } else {
            this.currentTime.dayname = this.getWeekDayName();
        }
    }
});

eventClock.tick_wrapper();

export default eventClock
