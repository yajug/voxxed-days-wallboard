import rooms from '@/data/rooms.json'

const roomService = Object.create({
    getRooms() {
        return rooms;
    },

    getRoom(id) {
        console.log('Getting data for room', id);
        let rooms = this.getRooms();

        let result = null;

        for (let i in rooms) {
            let room = rooms[i];
            // noinspection EqualityComparisonWithCoercionJS
            if (room.id == id) {
                result = room;
                break;
            }
        }

        return result;
    }
});

export default roomService
