import {createApp} from 'vue'
import App from './App.vue'
import router from './router'
import eventClock from './services/eventclock'
import roomService from './services/roomservice'
import slotService from './services/slotservice';
import speakerService from './services/speakerservice';

const app = createApp(App)

app.use(router)
app.provide('eventClock', eventClock)
app.provide('roomService', roomService)
app.provide('slotService', slotService)
app.provide('speakerService', speakerService)
app.mount('#app')
