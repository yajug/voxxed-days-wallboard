export function timeAsString(d) {
    let now = d;
    let hh = now.getHours();
    let mm = now.getMinutes();

    if (mm < 10) mm = '0' + mm;
    if (hh < 10) hh = '0' + hh;

    return `${hh}:${mm}`
}

export function weekDayName(d) {
    return d.toLocaleDateString('en-US', {weekday: 'long'}).toLocaleLowerCase();
}
